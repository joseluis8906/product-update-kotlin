package com.inventory.productupdate.controllers

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpStatus
import com.inventory.productupdate.models.Product
import com.inventory.productupdate.repositories.ProductRepository
import java.util.Optional
import com.mongodb.client.result.UpdateResult

@RestController("/api/v1/products")
class ProductController(private val productRepository: ProductRepository) {
  @PutMapping()
  fun updateProduct(@RequestBody product: Product): ResponseEntity<String> {
    val result: UpdateResult = this.productRepository.update(product);
    if (result.getModifiedCount().toInt() == 1) {
      return ResponseEntity<String>("", HttpStatus.OK)
    } else {
      return ResponseEntity<String>("", HttpStatus.CONFLICT)
    }
  }
}