package com.inventory.productupdate.repositories

import com.inventory.productupdate.models.Product
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Service
import java.util.Optional
import com.mongodb.client.result.UpdateResult

@Service
class ProductRepository(private val mongoOperations: MongoOperations ) {
  companion object {
    public val COLLECTION: String = "products";
  }
  
  public fun findOne(product: Product): Product? {
    return mongoOperations.findOne(
      Query.query(Criteria.where("name").regex(product.name)),
      Product::class.java,
      COLLECTION
    );
  }

  public fun update(product: Product): UpdateResult {
    return mongoOperations.updateFirst(
      Query.query(Criteria.where("name").regex(product.name)),
      Update().set("price", product.price),
      Product::class.java,
      COLLECTION
    );
  }
}