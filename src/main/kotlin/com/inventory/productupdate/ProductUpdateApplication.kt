package com.inventory.productupdate

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProductUpdateApplication

fun main(args: Array<String>) {
	runApplication<ProductUpdateApplication>(*args)
}
